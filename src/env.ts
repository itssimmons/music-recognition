export const OPENAI_API_KEY = import.meta.env.PUBLIC_OPENAI_API_KEY;
export const SPOTIFY_API_KEY = import.meta.env.PUBLIC_SPOTIFY_API_KEY;
