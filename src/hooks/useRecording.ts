import WaveSurfer from "wavesurfer.js";
import { createEffect, createSignal } from "solid-js";
import { transcriptAudio } from "../components/services/transcription.service";

const constraints = {
  audio: true,
  video: false,
  preferCurrentTab: true,
};

let chunks: any[] = [];

export default function () {
  const [result, setResult] = createSignal<{ blob: Blob | null; uri: string }>({
    blob: null,
    uri: "",
  });
  const [recordState, setRecordState] = createSignal<
    "inactive" | "recording" | "recorded"
  >("inactive");
  const [source, setSource] = createSignal<MediaRecorder | null>(null);
  const [stream, setStream] = createSignal<MediaStream | null>(null);

  const record = () => {
    setRecordState("recording");

    (async () => {
      const stream = await navigator.mediaDevices.getUserMedia(constraints);
      const src = new MediaRecorder(stream, {
        mimeType: "audio/webm;codecs=opus",
      });

      src.start();

      src.addEventListener("dataavailable", (e) => {
        chunks = [e.data];
      });

      src.addEventListener("stop", async (e) => {
        const newBlob = new Blob(chunks, { type: "audio/webm;codecs=opus" });
        const url = URL.createObjectURL(newBlob);
        setResult({ blob: newBlob, uri: url });
      });

      setSource(src);
      setStream(stream);
    })();
  };

  const stop = () => {
    source()?.stop();
    stream()
      ?.getTracks()
      .forEach((track) => track.stop());
    setRecordState("recorded");
  };

  return { result: result, record, stop, state: recordState };
}
