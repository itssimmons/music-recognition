import { createEffect, createSignal } from "solid-js";
import useRecording from "../../hooks/useRecording.js";
import { AudioIcon, Loading } from "../Icons";
import TrackPreview from "../TrackPreview";
import { DeezerResponse, searchMusic } from "../services/deezer.service";
import { transcriptAudio } from "../services/transcription.service";
import "./index.css";

export default function () {
  const { record, result, stop, state } = useRecording();
  const [dzData, setDzData] = createSignal<DeezerResponse | null>(null);
  const [transcription, setTranscription] = createSignal(
    "Press the mic to start!"
  );

  createEffect(async () => {
    if (state() === "recorded") {
      const transcriptionDetected = await transcriptAudio({
        blob: result().blob!,
      });
      const musicFound = await searchMusic({ search: transcriptionDetected });

      setTranscription(() => transcriptionDetected);
      setDzData(() => musicFound);
    }

    return audioIcon();
  });

  const [audioIcon, setAudioIcon] = createSignal<"record" | "stop">("record");

  const handleRecord = (e?: any) => {
    if (audioIcon() === "record") {
      setAudioIcon("stop");
      record?.();
    } else {
      setAudioIcon("record");
      stop?.();
    }
  };

  return (
    <div id="x4345sd">
      <TrackPreview {...dzData()} />
      <section class="audio--section">
        {["inactive", "recorded"].includes(state()) && <p>{transcription()}</p>}
        {state() === "recording" && <Loading />}
        <div class="audio--record">
          <span id="mic" onClick={handleRecord}>
            <AudioIcon type={audioIcon()} />
          </span>
          <div id="audio">
            <div id="waves"></div>
          </div>
        </div>
      </section>
    </div>
  );
}
