import { createEffect, createSignal, onCleanup } from "solid-js";
import WaveSurfer from "wavesurfer.js";
import { AudioControlIcon } from "../Icons";
import "./index.css";

export default function (props: any) {
  const [audioIcon, setAudioIcon] = createSignal<"play" | "pause">("pause");
  let control: HTMLSpanElement | undefined;
  let img: HTMLImageElement | undefined;

  createEffect(() => {
    const trackPreviewDiv = document.querySelector("div#track-preview");

    if (props?.preview) {
      const musicDiv = document.createElement("div");
      musicDiv.id = "music";

      trackPreviewDiv?.appendChild(musicDiv);

      const wavesurfer = WaveSurfer.create({
        container: musicDiv,
        waveColor: "#ddd",
        progressColor: "#373737",
        barWidth: 4,
        responsive: true,
        height: 90,
        barRadius: 4,
      });

      wavesurfer.load(props!.preview);

      wavesurfer.on("finish", () => {
        setAudioIcon("pause");
      });

      control!.onclick = () => {
        wavesurfer.playPause();
      };
    }

    onCleanup(() => {
      let child = trackPreviewDiv?.lastElementChild;
      while (child) {
        trackPreviewDiv?.removeChild(child);
        child = trackPreviewDiv?.lastElementChild;
      }
    });
  });

  const handleControl = () => {
    setAudioIcon((icon) => (icon === "play" ? "pause" : "play"));
  };

  return (
    <section class="track--preview">
      {!props?.preview && (
        <p class="starting--text">Record an audio to search a song 🎼</p>
      )}
      {props?.preview && (
        <>
          <img
            ref={img}
            id="cover"
            src={props.album.cover_big}
            alt="album cover"
          />
          <section class="music--metadata">
            <div id="track">
              <div class="info">
                <span ref={control} id="control" onClick={handleControl}>
                  <AudioControlIcon type={audioIcon()} />
                </span>
                <div class="description">
                  <h5>{props.artist.name}</h5>
                  <h5>{props.title}</h5>
                </div>
              </div>
              <div class="duration">
                <span class="badget">Sample</span>
                <p>{props.duration}s</p>
              </div>
            </div>
            <div id="track-preview"></div>
          </section>
        </>
      )}
    </section>
  );
}
