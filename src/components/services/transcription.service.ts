import axios from "axios";
import { createSignal } from "solid-js";
import { OPENAI_API_KEY } from "../../env";

export type TranscriptAudioParams = {
  blob: Blob
}

export const transcriptAudio = async (data: TranscriptAudioParams) => {
  const file = new File([data.blob], "audio-transcription.webm", {
    type: "audio/webm",
  });

  const form = new FormData();

  form.append("file", file);
  form.append("model", "whisper-1");
  form.append("response_format", "text");
  form.append("temperature", "0.2");
  form.append("languague", "es");

  const response = await axios.post(
    `https://api.openai.com/v1/audio/transcriptions`,
    form,
    {
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${OPENAI_API_KEY}`,
      },
    }
  );

  return await response.data;
};
