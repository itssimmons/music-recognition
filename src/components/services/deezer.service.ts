import axios from "axios";
import { createSignal } from "solid-js";
import { OPENAI_API_KEY, SPOTIFY_API_KEY } from "../../env";

export type DeezerParams = {
  search: string;
  limit?: number;
  output?: "json" | "jsonp";
};

interface DeezerAlbum {
  id: number;
  title: string;
  cover: string;
  cover_small: string;
  cover_medium: string;
  cover_big: string;
  cover_xl: string;
  md5_image: string;
  tracklist: string;
  type: string;
}

interface DeezerArtist {
  id: number;
  name: string;
  link: string;
  picture: string;
  picture_small: string;
  picture_medium: string;
  picture_big: string;
  picture_xl: string;
  tracklist: string;
  type: string;
}

export interface DeezerResponse {
  id: number;
  readable: boolean;
  title: string;
  title_short: string;
  title_version: string;
  link: string;
  duration: string;
  rank: string;
  explicit_lyrics: boolean;
  explicit_content_lyrics: number;
  explicit_content_cover: number;
  preview: string;
  md5_image: string;
  artist: DeezerArtist;
  album: DeezerAlbum;
}

export const searchMusic = async (
  data: DeezerParams
): Promise<DeezerResponse | null> => {
  const search = window.encodeURIComponent(data.search.trim());
  const params = {
    q: search,
    limit: 1,
    output: "json",
  };

  const response = await axios.get(`https://api.deezer.com/search`, {
    params,
    headers: {
      "Content-Type": "application/json",
    },
  });

  return (await response.data?.data?.[0]) || null;
};
